package patchara.albumproject.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.albumproject.helper.DatabaseHelper;
import patchara.albumproject.model.ReportSale;

public class SaleDao {

    public List<ReportSale> getDayReport() {
        ArrayList<ReportSale> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿SELECT strftime(\"%Y-%m-%d\", InvoiceDate) as period, SUM(Total) as total   FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport(int year) {
        ArrayList<ReportSale> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿SELECT strftime(\"%Y-%m\", InvoiceDate) as period, SUM(Total) as total   FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "WHERE strftime(\"%Y\", InvoiceDate)=\"" + year + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
