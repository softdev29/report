package patchara.albumproject.poc;

import java.util.List;
import patchara.albumproject.model.ReportSale;
import patchara.albumproject.service.ReportService;

public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
//      Day
//        List<ReportSale> report = reportService.getReportSaleByDay();
//      Month
        List<ReportSale> report = reportService.getReportSaleByMonth(2013);
        for (ReportSale r : report) {
            System.out.println(r);
        }
    }
}
