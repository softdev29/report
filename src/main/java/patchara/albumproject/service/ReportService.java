
package patchara.albumproject.service;

import java.util.List;
import patchara.albumproject.dao.SaleDao;
import patchara.albumproject.model.ReportSale;

public class ReportService {
    public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    
    public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
}
